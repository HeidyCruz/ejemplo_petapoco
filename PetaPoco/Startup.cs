﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PetaPoco.Startup))]
namespace PetaPoco
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
