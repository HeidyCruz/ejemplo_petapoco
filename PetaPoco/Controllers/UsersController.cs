﻿using PetaPoco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PetaPoco.Controllers
{
    public class UsersController : Controller
    {
       
        Database db = new PetaPoco.Database("Postgresql");
        public ActionResult Index()
        {
            ViewData.Model = db.Query<User>("select * from userinfo");
            return View();
        }

        public ActionResult Details(int id)
        {
            ViewData.Model = db.SingleOrDefault<User>("select * from userinfo where id=@0", id);
            return View();
        }


        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(User user)
        {
            try
            {
                db.Insert(user);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            ViewData.Model = db.SingleOrDefault<User>("where id=@0", id);
            return View();
        }


        [HttpPost]
        public ActionResult Edit(User user)
        {
            try
            {
                db.Update(user);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            ViewData.Model = db.SingleOrDefault<User>("where id=@0", id);
            return View();
        }


        [HttpPost]
        public ActionResult Delete(User user)
        {
            try
            {
                db.Delete(user);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
